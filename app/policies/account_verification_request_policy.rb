# frozen_string_literal: true

class AccountVerificationRequestPolicy < ApplicationPolicy
  def create?
    AccountVerificationRequest.where(account: current_account).count == 0
  end

  def index?
    admin?
  end

  def approve?
    admin?
  end

  def reject?
    admin?
  end
end
