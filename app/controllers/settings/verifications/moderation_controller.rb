class Settings::Verifications::ModerationController < Admin::BaseController
	def index
		authorize :account_verification_request, :index?
		@verification_requests = AccountVerificationRequest.all
	end

	def approve
		authorize :account_verification_request, :approve?
		verification_request = AccountVerificationRequest.find params[:id]

		# Mark user as verified
		account = verification_request.account
		account.is_verified = true
		account.save()

		# Notify user
		UserMailer.verification_approved(account.user).deliver_later!

		# Remove all traces
		verification_request.destroy()

		# Redirect back to the form with a proper message
		redirect_to settings_verifications_moderation_url, notice: I18n.t('verifications.moderation.approved_msg')
	end

	def reject
		authorize :account_verification_request, :reject?
		verification_request = AccountVerificationRequest.find params[:id]

		# Remove all traces
		verification_request.destroy()

		# Redirect back to the form with a proper message
		redirect_to settings_verifications_moderation_url, notice: I18n.t('verifications.moderation.rejected_msg')
	end
end
