# frozen_string_literal: true

class AboutController < ApplicationController
  layout 'public'

  before_action :set_instance_presenter, only: [:show, :more, :terms, :privacy, :dmca]

  def show
    if user_signed_in?
      redirect_to "/home"
    else
      @hide_navbar = false
    end
  end

  def more; end
  def terms; end
  def privacy; end
  def dmca; end

  private

  def set_instance_presenter
    @instance_presenter = InstancePresenter.new
  end
end
