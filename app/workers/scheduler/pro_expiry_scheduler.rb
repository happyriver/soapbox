# frozen_string_literal: true

class Scheduler::ProExpiryScheduler
  include Sidekiq::Worker

  sidekiq_options unique: :until_executed, retry: 0

  def perform
    accounts = Account.where(is_pro: true).where("pro_expires_at <= :now", now: Time.now.utc)

    accounts.each do |a|
      a.end_pro!
      Rails.logger.info("Pro expired for @#{a.acct}")
    end
  end
end
