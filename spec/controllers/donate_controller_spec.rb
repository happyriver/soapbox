require 'rails_helper'
require 'webmock/rspec'

RSpec.describe DonateController, type: :controller do
  render_views

  let(:user) { Fabricate(:user) }

  before do
    stub_request(:post, "https://api.stripe.com/v1/customers/cus_FzJt00uENozE77").to_return(status: 200)
    stub_request(:post, "https://api.stripe.com/v1/customers/cus_G1vDXqA8Oxe8Gg").to_return(status: 200)
    stub_request(:post, "https://api.stripe.com/v1/customers").to_return(status: 200, body: File.read("spec/support/fixtures/stripe/cus_1.json"))
    stub_request(:get, "https://api.stripe.com/v1/subscriptions?customer=cus_FzJt00uENozE77&expand%5B%5D=data.default_payment_method&plan=plan_monthly_donation").to_return(status: 200, body: File.read("spec/support/fixtures/stripe/sub_list_1.json"))
    stub_request(:get, "https://api.stripe.com/v1/subscriptions?customer=cus_G1vDXqA8Oxe8Gg&expand%5B%5D=data.default_payment_method&plan=plan_monthly_donation").to_return(status: 200, body: File.read("spec/support/fixtures/stripe/sub_list_empty.json"))
    stub_request(:post, "https://api.stripe.com/v1/checkout/sessions").to_return(status: 200, body: File.read("spec/support/fixtures/stripe/cs_1.json"))

    sign_in user, scope: :user
  end

  describe 'GET #index' do
    context 'when user has no subscriptions' do
      before do
        sign_in Fabricate(:user, account: Fabricate(:account, stripe_cus_id: "cus_G1vDXqA8Oxe8Gg"))
        get :index
      end

      it 'returns http success' do
        expect(response).to have_http_status(200)
      end

      it 'displays the payment form' do
        expect(response.body).to include("<form class='payform'>")
      end
    end

    context 'when user is a monthly subscriber' do
      before do
        sign_in Fabricate(:user, account: Fabricate(:account, stripe_cus_id: "cus_FzJt00uENozE77"))
        get :index
      end

      it 'returns http success' do
        expect(response).to have_http_status(200)
      end

      it 'displays the manage pro page' do
        expect(response.body).to include("<div class='subs'>")
      end
    end
  end

  describe 'POST #stripe' do
    before do
      sign_in user, scope: :user
      post :stripe, params: { amount: 1337 }
      user.reload
    end

    it 'returns http success' do
      expect(response).to have_http_status(200)
    end

    it 'returns a Stripe Checkout token' do
      cs = Stripe::Checkout::Session.construct_from(JSON.parse(File.read("spec/support/fixtures/stripe/cs_1.json"), symbolize_names: true))
      expect(cs.id).to eq(response.body)
      expect(cs.customer).to eq(user.account.stripe_cus_id)
      expect(cs.display_items[0].plan.id).to eq("plan_monthly_donation")
      expect(cs.display_items[0].quantity).to eq(1337)
    end
  end
end
