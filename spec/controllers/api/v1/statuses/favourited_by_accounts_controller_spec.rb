require 'rails_helper'

RSpec.describe Api::V1::Statuses::FavouritedByAccountsController, type: :controller do
  render_views

  let(:user)  { Fabricate(:user, account: Fabricate(:account, username: 'alice')) }
  let(:app)   { Fabricate(:application, name: 'Test app', website: 'http://testapp.com') }
  let(:token) { Fabricate(:accessible_access_token, resource_owner_id: user.id, application: app, scopes: 'read:accounts') }

  describe 'GET #index' do
    let(:status) { Fabricate(:status, account: user.account) }

    before do
      Fabricate(:favourite, status: status)
    end

    it 'returns http forbidden' do
      get :index, params: { status_id: status.id, limit: 1 }
      expect(response).to have_http_status(403)
    end
  end
end
