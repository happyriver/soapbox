require 'rails_helper'

describe 'Billing Events' do
  before do
    stub_request(:post, "https://api.stripe.com/v1/customers/cus_FzJt00uENozE77").to_return(status: 200)
    stub_request(:get, "https://api.stripe.com/v1/subscriptions/sub_FzLfPG7ifJxGc5").to_return(status: 200, body: File.read("spec/support/fixtures/stripe/sub_1.json"))
  end

  def bypass_event_signature(payload)
    event = Stripe::Event.construct_from(JSON.parse(payload, symbolize_names: true))
    expect(Stripe::Webhook).to receive(:construct_event).and_return(event)
  end

  describe 'invoice.payment_succeeded' do
    let(:payload) { File.read("spec/support/fixtures/stripe/evt_invoice_payment_succeeded.json") }
    before(:each) { bypass_event_signature payload }

    it 'gives user pro' do
      Fabricate(:user, account: Fabricate(:account, stripe_cus_id: "cus_FzJt00uENozE77", is_pro: false))
      post '/webhooks/stripe', params: payload
      expect(response).to have_http_status(200)

      event = Stripe::Event.construct_from(JSON.parse(payload, symbolize_names: true))
      invoice = event.data.object

      account = Account.find_by(stripe_cus_id: invoice.customer)
      expect(account.is_pro).to be true
    end
  end

  describe 'customer.subscription.deleted' do
    let(:payload) { File.read("spec/support/fixtures/stripe/evt_customer_subscription_deleted.json") }
    before(:each) { bypass_event_signature payload }

    it 'removes user pro' do
      Fabricate(:user, account: Fabricate(:account, stripe_cus_id: "cus_FzJt00uENozE77", is_pro: true))
      post '/webhooks/stripe', params: payload
      expect(response).to have_http_status(200)

      event = Stripe::Event.construct_from(JSON.parse(payload, symbolize_names: true))
      subscription = event.data.object

      account = Account.find_by(stripe_cus_id: subscription.customer)
      expect(account.is_pro).to be false
    end
  end

end
