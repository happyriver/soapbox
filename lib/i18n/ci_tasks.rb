# lib/i18n/ci_tasks.rb
require 'i18n/tasks/commands'

module I18n::Tasks
  module Interpolations

    def missing_interpolations(locales: nil, base_locale: nil) # rubocop:disable Metrics/AbcSize
      locales ||= self.locales
      base_locale ||= self.base_locale
      result = empty_forest

      data[base_locale].key_values.each do |key, value|
        next if !value.is_a?(String) || ignore_key?(key, :missing_interpolations)
        base_vars = Set.new(value.scan(VARIABLE_REGEX))
        (locales - [base_locale]).each do |current_locale|
          node = data[current_locale].first.children[key]
          next unless node&.value&.is_a?(String)
          if (Set.new(node.value.scan(VARIABLE_REGEX)) - base_vars).length > 0
            result.merge!(node.walk_to_root.reduce(nil) { |c, p| [p.derive(children: c)] })
          end
        end
      end

      result.each { |root| root.data[:type] = :missing_interpolations }
      result
    end
  end
end

module I18n
  module Tasks
    module Reports
      class Terminal < Base # rubocop:disable Metrics/ClassLength
        def missing_interpolations_title(forest)
          "Missing interpolations (#{forest.leaves.count || '∅'})"
        end

        def missing_interpolations(forest = task.missing_interpolations)
          if forest.present?
            print_title missing_interpolations_title(forest)
            show_tree(forest)
          else
            print_success "No missing interpolations found!"
          end
        end
      end
    end
  end
end


module I18nCITasks
  include ::I18n::Tasks::Command::Collection

  cmd :check_missing_interpolations,
      pos: '[locale ...]',
      desc: "check that all interpolation variables used in locales are used in base locale",
      args: %i[locales out_format]

 def check_missing_interpolations(opt = {})
   forest = i18n.missing_interpolations(opt.slice(:locales, :base_locale))
          print_forest forest, opt, :missing_interpolations
          :exit_1 unless forest.empty?
 end
end

I18n::Tasks.add_commands I18nCITasks
