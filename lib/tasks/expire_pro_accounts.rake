# frozen_string_literal: true

task expire_pro_accounts: 'gabsocial:expire_pro_accounts'

namespace :gabsocial do
  desc 'End pro for expired accounts.'
  task :expire_pro_accounts => :environment do
    accounts = Account.where(is_pro: true).where("pro_expires_at <= :now", now: Time.now.utc)

    accounts.each do |a|
      a.end_pro!
      puts("Pro expired for @#{a.acct}")
    end
  end
end
