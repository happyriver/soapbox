from fabric import task
from mastodon import Mastodon


def notify(c):
    with c.cd("live"):
        head = c.run("git rev-parse HEAD").stdout.strip()
        url = f"https://gitlab.com/soapbox-pub/soapbox/commit/{head}"

    mastodon = Mastodon(
        access_token = "monitoring_usercred.secret",
        api_base_url = "https://spinster.xyz"
    )
    mastodon.toot(f"An update to Spinster was just deployed! ({head[:8]}) {url}")


@task
def deploy(c):
    with c.cd("live"):
        c.run("git fetch origin master")
        c.run("git reset --hard origin/master")
        c.run("bundle install")
        c.run("bundle exec rails assets:precompile")
        c.run("DB_PORT=5432 bundle exec rails db:migrate")
    c.local(f"ssh root@{c.host} systemctl restart gabsocial-web.service")
    c.run("sleep 15")
    notify(c)
